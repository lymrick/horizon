<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'MzEiB/aR0zIG24/1fiH7x7mtTvcI+Lv+o6mUGWj7a2DNp96Kh3DpbWONCw4PTEhPkt6QILF7YSz5LIhnvNZs9w==');
define('SECURE_AUTH_KEY',  'gzjfNlcI6GGgEvbS6cpfsXhOUQuDY46VG7rW+69WZIPguG212lGpPdY5N4MH2kI7ydotYP/eBvo11z7FT18ANw==');
define('LOGGED_IN_KEY',    '+zpQdhrov6yLBWtBpT/PA7T2UVOFkVwGYAxTkA7wwtHz/h8CaHgS+SiBEYEoVAlsvFOLRlNU4j1sOHU1fq/U5w==');
define('NONCE_KEY',        'obQG7m3onmVHjzUIcRqxgDuVkhL1YMPVpVYaCVVktU++sz5le0MnBFOSygLrpMGkvZ5m80voNow+huQyAztfIw==');
define('AUTH_SALT',        'of6U/C5r44VpnavTF7QHhrX4NbGaDQV34OeDU636wlQneCHs+n4Vfhy+q8PIswx6rpJ2x0fNHrGtxxgBYPqtBA==');
define('SECURE_AUTH_SALT', 'eoweqirhYqij9aHH7T/GBJPaQdWy/HvUx+Lqf7sgXbc+e5LKWskrjUNfsqpONeA54jPXZWCNAHm3mfk9CYQEpQ==');
define('LOGGED_IN_SALT',   'fgkPq69kGY9AxCmkCyzjAih+Xa1SVYq0MjYDiSsta+t8v/yFWB0+M0kDRZExNLvCLgJm8sQnZ4lX0sbFEbAj5w==');
define('NONCE_SALT',       'Oep7AATS0BIdyBS/YIM0g0qH0sKm/A9Hr0CG9BZntaEsmrSk9qhs99v/mR4EaV71AUVIKQcI4nP9O9u4V0+OkQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';





/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
