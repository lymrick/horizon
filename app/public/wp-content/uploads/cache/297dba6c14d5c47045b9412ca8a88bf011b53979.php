<?php $__env->startSection('content'); ?>
  <?php while(have_posts()): ?> <?php (the_post()); ?>
  <div class="page-container">
  <div class="grid-x grid-padding-x align-center text-center">
      <div class="small-12 large-6">
          <?php (the_content()); ?>
      </div>
  </div>
  <?php ($paged = get_query_var('paged') ? get_query_var('paged') : 1); ?>
  <?php ($events_query = new WP_Query(['post_type' => 'events', 'meta_key' => 'event_start_date', 'orderby' => 'meta_value', 'order' => 'ASC','posts_per_page' => 8, 'paged' => $paged])); ?>
  <section id="events-list">
      <div class="grid-x grid-padding-x align-stretch" id="events">
          <?php while($events_query -> have_posts()): ?> <?php ($events_query -> the_post()); ?>

          <div class="small-12 large-3 cell events-wrap">
              <div class="event card text-center">
                  <a href="<?php (the_permalink()); ?>">
                      <?php if(has_post_thumbnail()): ?>
                          <?php (the_post_thumbnail('staff-card')); ?>
                      <?php else: ?>
                          <img src="<?= App\asset_path('images/placeholder.png'); ?>">
                      <?php endif; ?>
                      <div class="event-meta">
                          <h4><?php (the_title()); ?></h4>
                          <?php ($start_date_raw = get_field('event_start_date')); ?>
                          <?php ($end_date_raw = get_field('event_end_date')); ?>
                          <h6 class="event-date">
                              <?php if($end_date_raw && $end_date_raw != $start_date_raw): ?>
                                  <?php echo e(get_field('event_start_date')); ?> - <?php echo e(get_field('event_end_date')); ?>

                              <?php else: ?>
                                  <?php echo e(get_field('event_start_date')); ?>

                              <?php endif; ?>
                          </h6>
                      </div>
                  </a>
              </div>
          </div>
          <?php endwhile; ?>
      </div>
  </section>


  <?php ($start_date_raw = get_field('event_start_date')); ?>
  <?php ($start_date = new DateTime($start_date_raw)); ?>
  <h2></h2>

  <?php endwhile; ?>
  <div class="grid-x grid-padding-x align-center">
      <div class="event-pagination">
          <div class="event-next"> <?php (next_posts_link('Next', $events_query -> max_num_pages)); ?></div>
          <div class="event-previous"> <?php (previous_posts_link('Previous', $events_query -> max_num_pages)); ?></div>
      </div>
  </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>