<?php $__env->startSection('content'); ?>
    <?php while(have_posts()): ?> <?php (the_post()); ?>
<div class="page-container" id="event-single">
    <div class="section-container" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%), url('<?php the_post_thumbnail_url( 'full' ) ?>') 50% 70%">
    <div class="grid-x align-middle">
        <div class="small-12 large-6 cell">
            <div class="left-wrapper text-center">
                <h2 class="section-title"><?php (the_title()); ?></h2>
            </div>
        </div>
        <div class="small-12 large-6 cell">
            <div class="right-container">
            <h5 class="event-date">Date: <?php echo e(get_field('event_start_date')); ?></h5>
            <h6 class="event-time">Event Time: <?php echo e(get_field('event_times')); ?></h6>
                <?php ($location = get_field('event_address')); ?>
                <h6>Address: <?php echo e($location['address']); ?></h6>
            <?php (the_content()); ?>
                <?php ($contact_name = get_field('event_contact_person')); ?>
                <?php if($contact_name): ?>
                <h6>Contact <?php echo e($contact_name); ?>

                    <?php ($contact_email = get_field('event_contact_person_email')); ?>
                <?php if($contact_email): ?>
                        at <a href="mailto:<?php echo e($contact_email); ?>"><?php echo e($contact_email); ?></a>
                    <?php endif; ?>
                    <?php endif; ?>
                </h6>
            </div>
        </div>
    </div>
    </div>
</div>
    <?php endwhile; ?>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>