<?php $__env->startSection('content'); ?>
    <?php while(have_posts()): ?> <?php (the_post()); ?>
    <div class="home-container" style="background: url('<?php the_post_thumbnail_url( 'full' ) ?>') 50% 0 no-repeat fixed">
        <div class="grid-x grid-padding-x align-center-middle">
            <div class="small-12 large-6 cell text-center">
                <img src="<?= App\asset_path('images/HorizonChurch_Logo.svg'); ?>">
                <h2 class="hero-text">Show and share the kindness of Jesus.</h2>
                <a href="#" class="join-us">JOIN US</a>
            </div>
        </div>
    </div>
    <?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>