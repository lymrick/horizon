<nav class="nav-primary">
    <?php if(has_nav_menu('primary_navigation')): ?>
        <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'menu vertical large-horizontal']); ?>

    <?php endif; ?>
</nav>