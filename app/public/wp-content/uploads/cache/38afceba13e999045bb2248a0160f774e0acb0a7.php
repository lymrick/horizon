<div class="title-bar hide-for-large">
  <a href="#" class="fa fa-navicon" data-open="main-menu"></a>
  <img src="<?= App\asset_path('images/horizon_mobile.svg'); ?>" alt="Logo" class="mobile-logo">
</div>

<header class="banner show-for-large">
  <div class="grid-x grid-padding-x align-middle">
    <div class="large-2 cell">
      <a class="brand" href="<?php echo e(home_url('/')); ?>"><img src="<?= App\asset_path('images/horizon_church_header.svg'); ?>" alt="Capstone Logo"></a>
    </div>
    <div class="large-8 cell">
      <?php echo $__env->make('partials.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
    <div class="large-2 cell">
      <div class="social">
        <a href="https://www.facebook.com/horizonchurchyyc/" target="_blank"><i class="fa fa-facebook-square"></i></a>
        
        <a href="http://instagram.com/horizonchurchyyc" target="_blank"><i class="fa fa-instagram"></i></a>
      </div>
    </div>
  </div>
</header>