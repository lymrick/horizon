<?php $__env->startSection('content'); ?>
    <?php while(have_posts()): ?> <?php (the_post()); ?>
    <div class="page-container">
        <section id="gatherings-section">
            <div class="grid-x text-center connect-hero align-center-middle" style="background: url(<?php echo e(get_field('gatherings_hero_image')); ?>) 50% 50%">
                <div class="medium-12 cell">
                    <h1 class="section-title"><?php echo e(get_field('gatherings_header')); ?></h1>
                </div>
            </div>

            <div class="grid-x align-center-middle text-center">
                <div class="medium-12 cell connect-content">
                    <?php echo e(the_field('gatherings_content')); ?>

                </div>
            </div>
        </section>

        <section id="groups-section">
            <div class="grid-x text-center connect-hero align-center-middle" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%), url(<?php echo e(get_field('community_groups_hero_image')); ?>) 50% 50%">
                <div class="medium-12 cell">
                    <h1 class="section-title"><?php echo e(get_field('community_groups_header')); ?></h1>
                </div>
            </div>

            <div class="grid-x align-center-middle text-center">

                <div class="medium-12 cell connect-content">
                    <?php echo e(the_field('community_groups_content')); ?>

                </div>
            </div>

            <?php (the_content()); ?>

        </section>

        <section id="groups-section">
            <div class="grid-x text-center connect-hero align-center-middle" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%), url(<?php echo e(get_field('faq_hero_image')); ?>) 50% 70%">
                <div class="medium-12 cell">
                    <h1 class="section-title"><?php echo e(get_field('faq_header')); ?></h1>
                </div>
            </div>

            <div class="grid-x align-center-middle text-center">
                <div class="medium-12 cell connect-content text-left">
                    <ul class="accordion" data-accordion>
                        <?php while(have_rows('faq')): ?> <?php (the_row()); ?>

                            <li class="accordion-item" data-accordion-item data-allow-all-closed="true">
                                <!-- Accordion tab title -->
                                <a href="#" class="accordion-title question"><?php echo e(get_sub_field('question')); ?></a>
                                <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                                <div class="accordion-content" data-tab-content>
                                    <p class="answer"><?php echo e(the_sub_field('answer')); ?></p>
                                </div>
                            </li>
                            <?php endwhile; ?>
                    </ul>

                </div>
            </div>
        </section>
    </div>
    <?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>