<?php $__env->startSection('content'); ?>
    <?php while(have_posts()): ?> <?php (the_post()); ?>
    <div class="page-container">
        <section id="who-we-are" class="section-container" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%), url('<?php echo e(get_field('section_1_image')); ?>') 50% 50% no-repeat;">
            <div class="grid-x grid-padding-x align-middle">
                <div class=" small-12 medium-6 cell left-container text-center">
                    <h2 class="section-title"><?php echo e(get_field('section_1_header')); ?></h2>
                </div>
                <div class="small-12 medium-6 cell right-container text-center">
                    <h2 class="content-title">We Are Horizon Church</h2>
                    <div class="about-content text-left">
                        <?php echo e(the_field('section_1_content')); ?>

                    </div>
                </div>
            </div>
        </section>

        <section id="our-story" class="section-container" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%), url(<?php echo e(get_field('section_3_image')); ?>)">
            <div class="grid-x align-middle">
                <div class="small-12 medium-6 cell left-container text-center">
                    <div class="left-wrapper">
                        <h2 class="section-title"><?php echo e(get_field('section_3_header')); ?></h2>
                    </div>
                </div>
                <div class="small-12 medium-6 cell right-container text-center">
                    <h2 class="content-title">We long to see people transformed by the good news of Jesus Christ.</h2>
                    <div class="about-content">
                        <?php echo e(the_field('section_3_content')); ?>

                    </div>

                </div>
            </div>
        </section>

        <section id="mission" class="section-container" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%), url(<?php echo e(get_field('section_4_image')); ?>) 50% 70%">
            <div class="grid-x align-middle">
                <div class="small-12 medium-6 cell left-container text-center">
                    <div class="left-wrapper">
                        <h2 class="section-title" id="mission-header"><?php echo e(get_field('section_4_header')); ?></h2>
                    </div>
                </div>
                <div class="small-12 medium-6 cell right-container text-center">
                    <p class="mission-summary">
                        <strong>Our mission</strong> is to <strong>show and share the kindness of Jesus</strong>. We make disciples who make disciples:
                        fully devoted followers of Jesus.
                    </p>
                    <p class="mission-summary"> <strong>Our vision</strong> is to be a church that is <strong>living in light of the kingdom</strong>. We
                        live into this reality as we engage in Jesus’ commission, express His compassion, and extend His community.</p>
                    <?php while(have_rows('value_card')): ?> <?php (the_row()); ?>
                        <div class="value-card-container">
                            <div class="value-card-divider">
                                <?php ($value_icon = get_sub_field('value_icon')); ?>
                                    <?php echo wp_get_attachment_image($value_icon, 'value-icon') ?>
                                    <h3><?php echo e(get_sub_field('value_title')); ?></h3>
                            </div>
                            <div class="value-card-content">
                                <?php echo e(the_sub_field('value_description')); ?>

                            </div>
                        </div>
                        <?php endwhile; ?>
                </div>
            </div>
        </section>

        <section id="mission" class="section-container" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%),url(<?php echo e(get_field('section_5_image')); ?>)">
            <div class="grid-x align-middle">
                <div class="small-12 medium-6 cell left-container text-center">
                    <h2 class="section-title"><?php echo e(get_field('section_5_header')); ?></h2>
                </div>
                <div class="small-12 medium-6 cell right-container text-center">
                    <div class="staff-card-container">
                        <div class="staff-card-divider">

                            <?php while(have_rows('staff_member')): ?> <?php (the_row()); ?>
                                <?php ($staff_image = get_sub_field('staff_image')); ?>
                                    <?php echo wp_get_attachment_image($staff_image, 'staff-card') ?>
                                    <div class="staff-title-box">
                                        <h3><?php echo e(get_sub_field('staff_name')); ?></h3>
                                        <h4 class="title-heading"><?php echo e(get_sub_field('staff_title')); ?></h4>
                                        <?php echo e(the_sub_field('staff_description')); ?>


                        </div>
                        <?php endwhile; ?>
                    </div>
                </div>
                </div>
            </div>
        </section>

        <section id="what-we-believe" class="section-container" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%), url(<?php echo e(get_field('section_2_image')); ?>)">
            <div class="grid-x align-middle">
                <div class="small-12 medium-6 cell left-container text-center">
                    <div class="left-wrapper">
                        <h2 class="section-title"><?php echo e(get_field('section_2_header')); ?></h2>
                    </div>
                </div>
                <div class="small-12 medium-6 cell right-container text-center">
                    <div class="about-content">
                        <?php echo e(the_field('section_2_content')); ?>

                    </div>

                </div>
            </div>
        </section>

        

        <ul class="bottom-nav hide-for-small-only">
            <li><a href="#who-we-are">Who We Are</a></li>
            <li><a href="#what-we-believe">What We Believe</a></li>
            <li><a href="#our-story">Our Story</a></li>
            <li><a href="#mission">Mission</a></li>
            <li><a href="#staff">Meet The Staff</a></li>
        </ul>
    </div>
    <?php endwhile; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>