<?php $__env->startSection('content'); ?>
  <?php global $paged; ?>
  <?php while(have_posts()): ?> <?php (the_post()); ?>
  <div class="page-container">
  <?php ($paged = get_query_var('paged') ? get_query_var('paged') : 1); ?>
  <?php ($sermon_query = new WP_Query(['post_type' => 'sermons', 'posts_per_page' => 9, 'paged' => $paged, 'nopaging' => false,])); ?>
  <div class="grid-container" id="sermons">
      <div class="grid-x align-center" id="sermon-wrapper">
          <?php while($sermon_query -> have_posts()): ?> <?php ($sermon_query -> the_post()); ?>
          <div class="small-12 medium-6 large-4 cell sermon-wrap">
              <div class="">
                  <div class="card wow animate-fade-in" data-animation-in="fade">
                      <a data-open="modal-<?php echo e(get_the_ID()); ?>">
                          <?php if(has_post_thumbnail()): ?>
                              <?php (the_post_thumbnail('card')); ?>
                          <?php endif; ?>
                      </a>
                  </div>
              </div>
          </div>
          <?php endwhile; ?>
      </div>


      <?php ($sermon_query2 = new WP_Query(['post_type' => 'sermons', 'posts_per_page' => -1])); ?>

      <?php while($sermon_query2 -> have_posts()): ?> <?php ($sermon_query2 -> the_post()); ?>
      <div class="sermon-modal reveal flipInY" id="modal-<?php echo e(get_the_ID()); ?>" data-reveal>
          <div class="modal-header text-center">
              <?php if(has_post_thumbnail()): ?>
                  <?php (the_post_thumbnail('full')); ?>
              <?php endif; ?>
              <div class="modal-header-content">
                  <h3><?php (the_title()); ?></h3>
                  <?php (the_content()); ?>
              </div>
          </div>
          <div class="sermons-wrapper">
              <?php while(have_rows('sermons')): ?> <?php (the_row()); ?>
              <?php if(get_sub_field('sermon_stream')): ?>
                  <div class="sermon">
                      <div class="sermon-header">
                          <h4><?php echo e(get_sub_field('sermon_title')); ?></h4>
                          <p class="meta">Preacher: <?php echo e(get_sub_field('sermon_preacher')); ?> - <i
                                      class="fa fa-calendar"></i> <?php echo e(get_sub_field('sermon_date')); ?></p>
                      </div>
                      <p class="sermon-description"><?php echo e(get_sub_field('sermon_description')); ?></p>
                      <audio src="<?php echo e(get_sub_field('sermon_stream')); ?>" type="audio/mpeg" preload="none" controls></audio>

                  </div>
              <?php endif; ?>
              <?php endwhile; ?>
          </div>
          <button class="close-button" data-close aria-label="Close reveal" type="button">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <?php endwhile; ?>
      <?php endwhile; ?>
      <?php (wp_reset_postdata()); ?>
      <div class="grid-x grid-padding-x align-center">
          <div class="sermon-pagination">
              <div class="sermon-next"> <?php (next_posts_link('Next', $sermon_query -> max_num_pages)); ?></div>
              <div class="sermon-previous"> <?php (previous_posts_link('Previous', $sermon_query -> max_num_pages)); ?></div>
          </div>
      </div>
  </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>