<?php
/**
 * Template Name: Sermon RSS
 */
header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
$more = 1;
echo '<?xml version="1.0" encoding="' . get_option('blog_charset') . '"?' . '>';
?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0" >
	<channel>
		<!-- general fields -->
		<title>Horizon Church</title>
		<link>http://wearehorizon.ca</link>
		<copyright>&#xA9; <?php echo date('Y')?> Horizon Church</copyright>
		<language>en-CA</language>
		<description>Horizon is committed to engaging in Jesus’ commission, embodying Jesus’ compassion and extending Jesus’
			community in the community of Brentwood and beyond.
		</description>
		<!-- itunes fields -->
		<itunes:author>Horizon Church</itunes:author>
		<itunes:image href="https://wearecapstone.ca/wp-content/uploads/2017/09/podcast.jpg"/>
		<itunes:summary>Horizon is committed to engaging in Jesus’ commission, embodying Jesus’ compassion and extending Jesus’
			community in the community of Brentwood and beyond.
		</itunes:summary>
		<itunes:category text="Religion &amp; Spirituality">
			<itunes:category text="Christianity"/>
		</itunes:category>
		<itunes:explicit>clean</itunes:explicit>
		<itunes:owner>

			<itunes:name>Simon Hull</itunes:name>

			<itunes:email>simon@simonhull.com</itunes:email>

		</itunes:owner>

		<?php
		$sermon_query = new WP_Query(['post_type' => 'sermons',  'posts_per_page' => -1]);
		while ($sermon_query->have_posts()) : $sermon_query->the_post();
			while(have_rows('sermons')) : the_row();
				$stream = get_sub_field( 'sermon_stream' );
				$date_raw = get_sub_field( 'sermon_date' );
				$description = get_sub_field( 'sermon_description' );
				$length = get_sub_field( 'sermon_duration' );
				$filesize = get_sub_field( 'sermon_filesize' );
				$date = new DateTime($date_raw);

				?>
				<item>
					<title><?php echo get_sub_field('sermon_title') ?></title>
					<pubDate><?php echo $date -> format('r') ?></pubDate>
					<guid><?php echo $stream ?></guid>
					<itunes:image href="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full') ?>" />
					<itunes:author>Horizon Church</itunes:author>
					<enclosure url="<?php echo $stream; ?>" length="<?php echo $filsesize ?>" type="audio/mpeg"/>
					<itunes:summary> <?php echo $description ?></itunes:summary>
					<itunes:duration><?php echo $length ?></itunes:duration>


				</item>
			<?php endwhile; ?>
		<?php endwhile; ?>
	</channel>
</rss>