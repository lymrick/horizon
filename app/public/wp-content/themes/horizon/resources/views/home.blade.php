{{--
  Template Name: Home Template
--}}

@extends('layouts.app')
@section('content')
    @while(have_posts()) @php(the_post())
    <div class="home-container" style="background: url('<?php the_post_thumbnail_url( 'full' ) ?>') 50% 0 no-repeat fixed">
        <div class="grid-x grid-padding-x align-center-middle">
            <div class="small-12 large-6 cell text-center">
                <img src="@asset('images/HorizonChurch_Logo.svg')">
                <h2 class="hero-text">Living in Light of the Kingdom</h2>
                <a href="#" class="join-us">JOIN US</a>
            </div>
        </div>
    </div>
    @endwhile
@endsection
