{{--
  Template Name: Contact Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
      <div class="page-container">
          <section id="contact" class="section-container" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%), url('{{get_field('contact_hero_image')}}') 50% 50% no-repeat;">
              <div class="grid-x grid-padding-x align-middle">
                  <div class=" small-12 medium-6 cell left-container text-center">
                      <h2 class="section-title">Contact Us</h2>
                  </div>
                  <div class="small-12 medium-6 cell right-container text-center">
                      <div class="contact-form">
                     @php(the_content())
                      </div>
                  </div>
              </div>
          </section>
      </div>
  @endwhile
@endsection
