{{--
  Template Name: About Template
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php(the_post())
    <div class="page-container">
        <section id="who-we-are" class="section-container" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%), url('{{get_field('section_1_image')}}') 50% 50% no-repeat;">
            <div class="grid-x grid-padding-x align-middle">
                <div class=" small-12 medium-6 cell left-container text-center">
                    <h2 class="section-title">{{ get_field('section_1_header') }}</h2>
                </div>
                <div class="small-12 medium-6 cell right-container text-center">
                    <h2 class="content-title">We Are Horizon Church</h2>
                    <div class="about-content text-left">
                        {{ the_field('section_1_content') }}
                    </div>
                </div>
            </div>
        </section>

        <section id="our-story" class="section-container" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%), url({{get_field('section_3_image')}})">
            <div class="grid-x align-middle">
                <div class="small-12 medium-6 cell left-container text-center">
                    <div class="left-wrapper">
                        <h2 class="section-title">{{ get_field('section_3_header') }}</h2>
                    </div>
                </div>
                <div class="small-12 medium-6 cell right-container text-center">
                    <h2 class="content-title">We long to see people transformed by the good news of Jesus Christ.</h2>
                    <div class="about-content">
                        {{ the_field('section_3_content') }}
                    </div>

                </div>
            </div>
        </section>

        <section id="mission" class="section-container" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%), url({{get_field('section_4_image')}}) 50% 70%">
            <div class="grid-x align-middle">
                <div class="small-12 medium-6 cell left-container text-center">
                    <div class="left-wrapper">
                        <h2 class="section-title" id="mission-header">{{ get_field('section_4_header') }}</h2>
                    </div>
                </div>
                <div class="small-12 medium-6 cell right-container text-center">
                    <p class="mission-summary">
                        <strong>Our mission</strong> is to <strong>show and share the kindness of Jesus</strong>. We make disciples who make disciples:
                        fully devoted followers of Jesus.
                    </p>
                    <p class="mission-summary"> <strong>Our vision</strong> is to be a church that is <strong>living in light of the kingdom</strong>. We
                        live into this reality as we engage in Jesus’ commission, express His compassion, and extend His community.</p>
                    @while(have_rows('value_card')) @php(the_row())
                        <div class="value-card-container">
                            <div class="value-card-divider">
                                @php($value_icon = get_sub_field('value_icon'))
                                    <?php echo wp_get_attachment_image($value_icon, 'value-icon') ?>
                                    <h3>{{get_sub_field('value_title')}}</h3>
                            </div>
                            <div class="value-card-content">
                                {{the_sub_field('value_description')}}
                            </div>
                        </div>
                        @endwhile
                </div>
            </div>
        </section>

        <section id="mission" class="section-container" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%),url({{get_field('section_5_image')}})">
            <div class="grid-x align-middle">
                <div class="small-12 medium-6 cell left-container text-center">
                    <h2 class="section-title">{{ get_field('section_5_header') }}</h2>
                </div>
                <div class="small-12 medium-6 cell right-container text-center">
                    <div class="staff-card-container">
                        <div class="staff-card-divider">

                            @while(have_rows('staff_member')) @php(the_row())
                                @php($staff_image = get_sub_field('staff_image'))
                                    <?php echo wp_get_attachment_image($staff_image, 'staff-card') ?>
                                    <div class="staff-title-box">
                                        <h3>{{get_sub_field('staff_name')}}</h3>
                                        <h4 class="title-heading">{{get_sub_field('staff_title')}}</h4>
                                        {{the_sub_field('staff_description')}}

                        </div>
                        @endwhile
                    </div>
                </div>
                </div>
            </div>
        </section>

        <section id="what-we-believe" class="section-container" style="background: linear-gradient(to bottom, rgba(10,10,10,0.4) 0%, rgba(61,60,61,0.4) 100%), url({{get_field('section_2_image')}})">
            <div class="grid-x align-middle">
                <div class="small-12 medium-6 cell left-container text-center">
                    <div class="left-wrapper">
                        <h2 class="section-title">{{ get_field('section_2_header') }}</h2>
                    </div>
                </div>
                <div class="small-12 medium-6 cell right-container text-center">
                    <div class="about-content">
                        {{ the_field('section_2_content') }}
                    </div>

                </div>
            </div>
        </section>

        {{--Bottom Navigation Bar--}}

        <ul class="bottom-nav show-for-large">
            <li><a href="#who-we-are">Who We Are</a></li>
            <li><a href="#what-we-believe">What We Believe</a></li>
            <li><a href="#our-story">Our Story</a></li>
            <li><a href="#mission">Mission</a></li>
            <li><a href="#staff">Meet The Staff</a></li>
        </ul>
    </div>
    @endwhile

@endsection
