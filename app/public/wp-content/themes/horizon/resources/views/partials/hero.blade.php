<section class="hero" style=" background: url('<?php the_post_thumbnail_url( 'full' ) ?>') 50% 0 no-repeat fixed"
         data-type="background" data-speed="10">

    <div class="grid-x text-center">
        <div class="small-12 cell">
            <h2>@php(the_title())</h2>
        </div>
    </div>
</section>