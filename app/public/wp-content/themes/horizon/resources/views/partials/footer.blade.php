<footer class="content-info">
  <div class="grid-x grid-padding-x  align-middle text-center">

    <div class="small-12 medium-6 large-6 cell">
      <div class="container">
        @php(dynamic_sidebar('sidebar-footer'))
      </div>
      <div class="horizon-meta">
        <img src="@asset('images/HorizonChurch_Logo.svg')">
      </div>

    </div>
    <div class="small-12 medium-6 large-6 cell">
      <div class="capstone-network">
        <a href="http://capstonenetwork.ca">
          <img src="@asset('images/capstone-network-white.svg')" alt="Capstone Network Logo">
          <h6>Part of the Capstone Network</h6>
        </a>
      </div>
    </div>

  </div>

  <div class="grid-x text-center">
    <div class="small-12 cell">
      <div class="attribution">
        <p>&copy; 2017 Horizon Church - Made with <i class="fa fa-heart"></i> by <a href="http://lymrick.com" target="_blank"> Lymrick</a></p>
      </div>
    </div>
  </div>
</footer>