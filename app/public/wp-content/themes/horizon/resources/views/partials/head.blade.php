<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/horizon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/horizon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/horizon/favicon-16x16.png">
  <link rel="manifest" href="/wp-content/themes/horizon/manifest.json">
  <link rel="mask-icon" href="/wp-content/themes/horizon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="/wp-content/themes/horizon/favicon.ico">
  <meta name="msapplication-config" content="/wp-content/themes/horizon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  @php(wp_head())
</head>
