<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')
  <body @php(body_class())>
    @php(do_action('get_header'))
    <div class="off-canvas-wrapper">
      <div class="off-canvas position-left" id="main-menu" data-off-canvas>
        @include('partials.menu')
      </div>
      <div class="off-canvas-content" data-off-canvas-content>
    @include('partials.header')
    <div class="wrap container" role="document">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif
      </div>
    </div>
    @php(do_action('get_footer'))
    @include('partials.footer')
    @php(wp_footer())
      </div>
    </div>
  </body>
</html>
