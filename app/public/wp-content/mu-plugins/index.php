<?php
add_action('init', 'register_capstone_post_types');
function register_capstone_post_types() {
	register_post_type( 'sermons', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Sermons' ), /* This is the Title of the Group */
			'singular_name' => __( 'Sermon' ), /* This is the individual type */
			'all_items' => __( 'All Sermons' ), /* the all items menu item */
			'add_new' => __( 'Add New' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Sermon Series' ), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Sermons' ), /* Edit Display Title */
			'new_item' => __( 'New Sermon' ), /* New Display Title */
			'view_item' => __( 'View Sermon' ), /* View Display Title */
			'search_items' => __( 'Search Sermons' ), /* Search Custom Type Title */
			'not_found' =>  __( 'Nothing found in the Database.' ), /* This displays if there are no entries yet */
			'not_found_in_trash' => __( 'Nothing found in Trash' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
		), /* end of arrays */
		       'description' => __( 'Our Sermons' ), /* Custom Type Description */
		       'public' => true,
		       'publicly_queryable' => true,
		       'exclude_from_search' => false,
		       'show_ui' => true,
		       'query_var' => true,
		       'menu_position' => 9, /* this is what order you want it to appear in on the left hand side menu */
		       'menu_icon' => 'dashicons-megaphone', /* the icon for the custom post type menu */
		       'rewrite'	=> array( 'slug' => 'sermons-list', 'with_front' => false ), /* you can specify its url slug */
		       'capability_type' => 'post',
		       'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			   'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields',)
		) /* end of options */
	); /* end of register post type */
	register_post_type( 'events', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Events' ), /* This is the Title of the Group */
			'singular_name' => __( 'Event' ), /* This is the individual type */
			'all_items' => __( 'All Events' ), /* the all items menu item */
			'add_new' => __( 'Add New Event' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Event' ), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Event' ), /* Edit Display Title */
			'new_item' => __( 'New Event' ), /* New Display Title */
			'view_item' => __( 'View Event' ), /* View Display Title */
			'search_items' => __( 'Search Events' ), /* Search Custom Type Title */
			'not_found' =>  __( 'Nothing found in the Database.' ), /* This displays if there are no entries yet */
			'not_found_in_trash' => __( 'Nothing found in Trash' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
		), /* end of arrays */
		       'description' => __( 'This is where all the Horizon Events go.' ), /* Custom Type Description */
		       'public' => true,
		       'publicly_queryable' => true,
		       'exclude_from_search' => false,
		       'show_ui' => true,
		       'query_var' => true,
		       'menu_position' => 9, /* this is what order you want it to appear in on the left hand side menu */
		       'menu_icon' => 'dashicons-calendar-alt', /* the icon for the custom post type menu */
		       'rewrite'	=> array( 'slug' => 'events-list', 'with_front' => false ), /* you can specify its url slug */
		       'has_archive' => 'event', /* you can rename the slug here */
		       'capability_type' => 'post',
		       'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			   'supports' => array( 'title', 'editor', 'thumbnail')
		) /* end of options */
	); /* end of register post type */
}
?>
